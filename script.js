// Завдання
// Реалізувати функцію, яка отримуватиме масив елементів і виводити їх на сторінку у вигляді списку. Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

// Технічні вимоги:

// Створити функцію, яка прийматиме на вхід масив і опціональний другий аргумент parent - DOM-елемент,
// до якого буде прикріплений список(по дефолту має бути document.body.
// Кожен із елементів масиву вивести на сторінку у вигляді пункту списку;

// Приклади масивів, які можна виводити на екран:
// ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
// ["1", "2", "3", "sea", "user", 23];

// Можна взяти будь-який інший масив.

// Необов'язкове завдання підвищеної складності:

// Додайте обробку вкладених масивів. Якщо всередині масиву одним із елементів буде ще один масив,
// виводити його як вкладений список.

// Приклад такого масиву:
// ["Kharkiv", "Kiev", ["Borispol", "Irpin"], "Odessa", "Lviv", "Dnieper"];

// Підказка: використовуйте map для обходу масиву та рекурсію, щоб обробити вкладені масиви.

// Очистити сторінку через 3 секунди. Показувати таймер зворотного відліку (лише секунди) перед очищенням сторінки.

"use strict";

function createList(list, parent = document.body) {
  let listContainer = document.createElement("ul"); // Creating <ul> container
  parent.append(listContainer); // Adding <ul> container to the end of body

  list.forEach((elem) => {
    if (Array.isArray(elem)) {
      // Checking whether the element is an array
      let nextParent = listContainer.lastChild; // Setting the last created element as the parent for inner list
      createList(elem, nextParent); // Recursion for inner array with updated parent element
    } else {
      let listItem = document.createElement("li"); // Creating the list item
      listItem.innerText = elem; // Setting the list item content
      listContainer.append(listItem); // Adding created list item to the end of current <ul> container
    }
  });
}

function clearPage(seconds) {
  let timerElement = document.createElement("h1"); // Creating the timer element
  document.body.append(timerElement); // Adding the timer element to the end of body

  let counter = seconds; // Setting timer counter

  const interval = setInterval(() => {
    // Function for timer updating
    timerElement.innerText = `Destroy in ${counter}`; // Live timer content
    counter--;
    if (counter < 0) {
      document.body.innerHTML = ""; // Clearing the page when the timer gets to 0
      clearInterval(interval);
    }
  }, 1000); // Timer interval
}

createList([
  "Kharkiv",
  "Kyiv",
  ["Boryspil", "Irpin", ["Romanivka", "Stoyanka", "Novoirpinska"]],
  "Odesa",
  "Lviv",
  "Dnipro",
]);

clearPage(3);
